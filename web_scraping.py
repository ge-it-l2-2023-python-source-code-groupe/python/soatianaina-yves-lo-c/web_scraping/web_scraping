from bs4 import BeautifulSoup
import requests

def scrape_quotes(theme):
    base_url = "https://quotes.toscrape.com"
    quotes_list = []
    page_number = 1
    next_page = True

    while next_page:
        url = f"{base_url}/page/{page_number}/"
        result = requests.get(url)
        doc = BeautifulSoup(result.text, "html.parser")

        quotes = doc.find_all(class_="quote")
        if not quotes:
            break

        for quote in quotes:
            text = quote.find(class_="text").get_text(strip=True)
            author = quote.find(class_="author").get_text(strip=True)
            tags = [tag.text for tag in quote.find_all(class_="tag")]
            if theme.lower() in ' '.join(tags).lower():
                quotes_list.append({"text": text, "author": author, "tags": tags})

        page_number += 1
        next_button = doc.find(class_="next")
        next_page = next_button is not None

    return quotes_list

user_theme = input("Enter a theme: ")

quotes = scrape_quotes(user_theme)

if quotes:
    print(f"Quotes related to '{user_theme}':")
    for index, quote in enumerate(quotes, start=1):
        print(f"{index}. '{quote['text']}' by {quote['author']} - Tags: {', '.join(quote['tags'])}")
else:
    print(f"No quotes found related to '{user_theme}'.")